import datetime

def getVariables():
    # generates unique email via adding current timestamp
    # Robot expects a dictionary output from this. In this case, parameters is used.
    now = datetime.datetime.now()
    now = now.strftime("%m%d-%Y%H%M%S")
    email = "testAutomated"+ now + "@gmail.com"
    parameters = {"generatedEmail": email}
    return parameters
