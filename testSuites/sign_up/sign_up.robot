*** Settings ***
Resource          sign_up_resource.robot
Resource          ../../resources/keywords/general_keywords.robot
Resource          ../../resources/variables/config.robot

*** Test Cases ***
Sign up using mobile number
    [Tags]      regression

    Open App
    Sign up using mobile number
    Enter verification code    ${passCode.firstDigt}    ${passCode.secDigt}    ${passCode.thirdDigt}    ${passCode.fourthDigt}    ${passCode.fifthDigt}   ${passCode.sixthDigt}
    Skip verify kyc modal
    Close All Apps

Sign up using email
    [Tags]    regression

    Open App
    Sign up using email
    Enter verification code    ${passCode.firstDigt}    ${passCode.secDigt}    ${passCode.thirdDigt}    ${passCode.fourthDigt}    ${passCode.fifthDigt}   ${passCode.sixthDigt}
    Skip verify kyc modal
    Close All Apps
