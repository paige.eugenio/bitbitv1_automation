*** Settings ***
Library           AppiumLibrary
Resource          ../../resources/locators/ios.robot
Variables        generateRandomPhoneNumber.py
Variables        generateEmailAddress.py

*** Variables ***
${MOBILE_NUMBER}    ${generatedMobile}
${EMAIL}            ${generatedEmail}

*** Keywords ***
Sign up using mobile number
    [Documentation]   Happy path

    Log              ${MOBILE_NUMBER}
    Wait Until Element Is Visible    ${homepageGetstartedButton}
    Click Element    ${homepageGetstartedButton}
    Wait Until Element Is Visible    ${signupMobilenumberTextfield}
    Input Text       ${signupMobilenumberTextfield}    ${MOBILE_NUMBER}
    Input Text       ${signupMobilenumberpasswordTextfield}    ${PASSWORD}
    Wait Until Element Is Visible    ${signupNextButton}
    Click Element    ${signupNextButton}
    Enter name

Sign up using email
    [Documentation]   Happy path

    Wait Until Element Is Visible    ${homepageGetstartedButton}
    Click Element    ${homepageGetstartedButton}
    Wait Until Element Is Visible    ${signupTakecashwhereeveryougoText}
    Click Element    ${signupTakecashwhereeveryougoText}
    Click Element    ${signupSignupwithemailinsteadLink}
    Wait Until Element Is Visible    ${signupEmailTextfield}
    Input Text    ${signupEmailTextfield}   ${EMAIL}
    Input Text    ${signupEmailpasswordTextfield}    ${PASSWORD}
    Wait Until Element Is Visible    ${signupNextButton}
    Click Element    ${signupNextButton}
    Enter name

Enter name
    [Documentation]     Hello

    Wait Until Element Is Visible    ${signupFirstnameTextfield}
    Input Text       ${signupFirstnameTextfield}    Test
    Input Text    ${signupLastnameTextfield}    Regression
    Tap           ${signupIntroduceyourselfText}
    Tap           ${signupCreateaccountButton}
