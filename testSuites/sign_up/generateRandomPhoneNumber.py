import random

def generateThirdDigit(secondDigit):
    newRange = ""
    if secondDigit == 0: #except 1-4
        newRange = str(0)
        for i in range (5,9):
             newRange = newRange + str(i)
    elif secondDigit == 1: #except 1, 3, 4
        newRange = str(0) + str(2)
        for i in range(5,9):
            newRange = newRange + str(i)
    elif secondDigit == 2: # except 4
        for i in range(0,3):
            newRange = newRange + str(i)
        for i in range(5,9):
            newRange = newRange + str(i)
    elif secondDigit == 3: # except 1 & 4
        newRange = str(0)
        for i in range(5,9):
            newRange = newRange + str(i)
        for i in range(2,3):
            newRange = newRange + str(i)
    elif secondDigit == 4: # except 0, 1, 4
        for i in range(5,9):
            newRange = newRange + str(i)
        for i in range(2,3):
            newRange = newRange + str(i)
    elif secondDigit == 5:# only 5-7 and 0
        newRange = str(0)
        for i in range(5,7):
            newRange = newRange + str(i)
    elif secondDigit == 6: # only 5-7 and 1
        newRange = str(1)
        for i in range(5,7):
            newRange = newRange + str(i)
    elif secondDigit == 7: # only 3-5, 7-9
        for i in range(3,5):
            newRange = newRange + str(i)
        for i in range(7,9):
            newRange = newRange + str(i)
    elif secondDigit == 8: # only 9
        newRange = str(9)
    elif secondDigit == 9:
        for i in range(5,9):# only 5-9
            newRange = newRange + str(i)
    thirdDigt = random.sample(newRange,1)
    return str(thirdDigt[0])

def getVariables():
    "generate valid ph number"
    generatedMobile = "9"

    newDigit = random.randint(1,9)
    generatedMobile = generatedMobile + str(newDigit) + generateThirdDigit(newDigit)

    for x in range (0, 7):
        newDigit = random.randint(0,9)
        generatedMobile = generatedMobile + str(newDigit)

    parameters = {"generatedMobile": generatedMobile}
    return parameters
