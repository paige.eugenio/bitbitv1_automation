*** Settings ***
Resource          log_in_resource.robot
Resource          ../../resources/keywords/general_keywords.robot


*** Test Cases ***
As existing verified user using email
    [Tags]      regression

    Open App
    Log in using email
    Close All Apps      

As an existing unverified user using mobile number
    [Tags]    regression

    Open App
    Log in using mobile number
    Close All Apps
# Log in as existing unverified user
# Log in as new unverified user
#log in as underreview
