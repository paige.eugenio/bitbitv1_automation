*** Settings ***
Library    AppiumLibrary
Resource   ../../resources/variables/config.robot
Resource   ../../resources/locators/ios.robot

*** Keywords ***

Log in using email

    Wait Until Element Is Visible    ${homepageAlreadyhaveanaccountLink}
    Tap               ${homepageAlreadyhaveanaccountLink}
    Input Text        ${loginEmailTextbox}          ${USERNAME}
    Input Password    ${loginEmailpasswordTextbox}       ${PASSWORD}
    Tap               ${loginLoginButton}
    Skip permission modal
    Log               UserStory # completed successfully

Log in using mobile number

    Wait Until Element Is Visible    ${homepageAlreadyhaveanaccountLink}
    Tap   ${homepageAlreadyhaveanaccountLink}
    Wait Until Element Is Visible    ${loginLoginwithemailButton}
    Tap    ${loginLoginwithemailButton}
    Input Text    ${loginLoginmobilenumberTextbox}    ${MOBILE_NUMBER}
    Input Text    ${loginLoginmobilepasswordTextbox}    ${PASSWORD_MOBILE}
    Tap    ${loginLoginButton}
    Skip permission modal
