*** Settings ***
Resource      ../variables/config.robot
Library       AppiumLibrary

*** Keywords ***
Open App
   Open Application    ${REMOTE_URL}    platformName=${PLATFORM_NAME}    platformVersion=${PLATFORM_VERSION}    deviceName=${DEVICE_NAME}    app=${APP_LOCATION}

Close All Apps
   Close All Applications

Enter verification code
    [Arguments]   ${firstDigt}    ${secDigt}    ${thirdDigt}    ${fourthDigt}   ${fifthDigt}    ${sixthDigt}

    Wait Until Element Is Visible    name=${firstDigt}
    Tap    name=${firstDigt}
    Tap    name=${secDigt}
    Tap    name=${thirdDigt}
    Tap    name=${fourthDigt}
    Tap    name=${fifthDigt}
    Tap    name=${sixthDigt}

Skip permission modal
    [Documentation]     Closes notification where user is asked to turn on permission upon first time use

    Wait Until Element Is Visible   ${dashboard_modalpermissionskipButton}          timeout=20s
    Tap     ${dashboard_modalpermissionskipButton}
    Page Should Contain Element   ${dashboardPricechartText}

Skip verify kyc modal
    [Documentation]   Modal is displayed upon logging in with incomplete kyc

    Wait Until Element Is Visible    ${dashboard_modalkycskipButton}      timeout=20s
    Tap    ${dashboard_modalkycskipButton} 
