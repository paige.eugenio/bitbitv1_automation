*** Variables ***
${homepageAlreadyhaveanaccountLink}       id=Already have an account?
${homepageGetstartedButton}               id=Get Started

${loginEmailTextbox}                      class=XCUIElementTypeTextField
${loginEmailpasswordTextbox}              class=XCUIElementTypeSecureTextField
${loginLoginButtontext}                   LOG IN
${loginLoginButton}                       id=${loginLoginButtontext}
${loginLoginwithemailButton}              id=Log in using Mobile No.
${loginLoginmobilenumberTextbox}          xpath=//XCUIElementTypeOther[2]/XCUIElementTypeTextField
${loginLoginmobilepasswordTextbox}        xpath=//XCUIElementTypeOther[3]/XCUIElementTypeSecureTextField


${signupMobilenumberTextfield}            xpath=//XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeTextField
${signupMobilenumberpasswordTextfield}    xpath=//XCUIElementTypeOther[2]/XCUIElementTypeOther[3]/XCUIElementTypeSecureTextField
${signupEmailpasswordTextfield}           xpath=//XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeSecureTextField
${signupEmailTextfield}                   xpath=//XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeTextField
${signupNextButton}                       id=Next
${signupTakecashwhereeveryougoText}       name=Take your cash wherever you go
${signupSignupwithemailinsteadLink}       id=Sign up with email instead
${signupFirstnameTextfield}               xpath=//XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeTextField
${signupLastnameTextfield}                xpath=//XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeTextField
${signupIntroduceyourselfText}            xpath=//XCUIElementTypeStaticText[@name="Introduce yourself!"]
${signupCreateaccountButton}              xpath=//XCUIElementTypeButton[@name="Create Account"]

${dashboard_modalpermissionskipButton}    id=SKIP
${dashboardPricechartText}                xpath=//XCUIElementTypeStaticText[@name="Bitcoin Price Chart"]
${dashboard_modalkycskipButton}           id=No thanks, I'll do it later.
